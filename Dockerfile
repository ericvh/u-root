FROM golang:latest as builder

COPY . /u-root
WORKDIR /u-root
RUN CGO_ENABLED=0 go build